import Vue from 'vue'
import App from '@/App'

describe('App.vue', () => {
    it('should containt correct data', () => {
        const Constructor = Vue.extend(App)
        const vm = new Constructor({
            data: {
                selectedTab: 1
            }
        }).$mount()
        expect(vm.$data.selectedTab).toEqual(1)
    })
})
