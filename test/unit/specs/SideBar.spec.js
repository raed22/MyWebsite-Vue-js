import Vue from 'vue'
import SideBar from '@/components/SideBar'

describe('SideBar.vue', () => {
    it('should render correct contents', () => {
        const Constructor = Vue.extend(SideBar)
        const vm = new Constructor({
            propsData: {
                selectedTab: 0
            },
            data: {
                name: "Raed Abdennadher"
            }
        }).$mount()
        expect(vm.$el.querySelector('span.w3-xlarge.w3-lobster').textContent)
            .toEqual('Raed Abdennadher')
    })
})
