import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios"

var GLOBAL_ID = 1
const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
]

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        comments: []
    },
    getters: {
        sortedComments(state) {
            return state.comments.sort((a, b) => {
                if (a.time > b.time) return -1
                if (a.time < b.time) return 1
                return 0
            })
        }
    },
    mutations: {
        addItem(state, playload) {
            const today = new Date()
            const date = today.getDate() + '-' + monthNames[today.getMonth()] + '-' + today.getFullYear()
            const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds()
            state.comments.push({
                id: GLOBAL_ID++,
                date: date,
                time: time,
                text: playload.comment,
                name: playload.name,
                photo: playload.photo
            })
        },
        removeItem(state, index) {
            state.comments.splice(index, 1)
        }
    },
    actions: {
        async addItemWithName(context, comment) {
            // Generate random user profile. Source: https://uifaces.co/api-docs
            await axios.get("https://uifaces.co/api?limit=1&offset=" + Math.floor(Math.random() * 151), {
                headers: {
                    "X-API-KEY": "ce7979e2b0314c9412b8132cb752ca"
                }
            })
                .then((response) => {
                    console.log(response.data)
                    context.commit('addItem', {
                        comment: comment,
                        name: response.data[0].name,
                        photo: response.data[0].photo
                    })
                })
                .catch((error) => console.log(error))
        }
    }
})