// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import store from './store'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: 'app',
  store,
  components: { App },
  data: {
    name: "Raed Abdennadher",
    field: "Software Engineer",
    basedOn: "Geneva",
    subtitle: "Master HES-SO student",
    content: "This is the content.",
  },
  computed: {
    title() {
      return "I am a " + this.field
    },
    year() {
      return new Date().getFullYear()
    }
  }
})
